

/**
 * @author Christos Zonios 2194
 */
public class Maze {

    private boolean mazeArray[][];      // 2d array representing a maze
    private int height;                 // # of rows
    private int width;                  // # of columns
    private StackOfStates stack;        // LIFO stack holding maze states
    private int solving;                // helper variable to hold position of final state in stack

    /**
     * Constructor method
     * @param height - No of rows
     * @param width - No of columns
     * @param mazeArray - input maze
     */
    public Maze(int height, int width, boolean[][] mazeArray) {
        this.mazeArray = mazeArray;
        this.height = height;
        this.width = width;
    }

    /**
     * @return true if maze is soluble. false otherwise.
     */
    public boolean solve(){

        // if entry or exit point are false the maze is insoluble
        if (!mazeArray[0][0] || !mazeArray[height-1][width-1])
            return false;

        // declare and initialize the stack
        stack = new StackOfStates(mazeArray);

        // initial state
        State curState = stack.getStack().get(0);
        int row;
        int col;
        solving = 0;

        /** Had this while(row != height-1 && col != width-1) but
         * loop finished when row was height-1 OR col was width-1
         * (of course I had initialized row and col above)
         * so I had to check the condition inside the while loop
         */
        while (true) {

            boolean popFlag = false;    // flag is true if we can move in any direction
            row = curState.getRow();
            col = curState.getColumn();

            //check if we can move left
            if ((curState.isLeft()) && !stack.inStack(row, (col - 1))) {
                State newState = new State(row, col - 1, mazeArray);
                newState.setCurrentMove(curState);
                stack.push(newState);
                popFlag = true;
            }
            //check if we can move right
            if ((curState.isRight()) && !stack.inStack(row, (col + 1))) {
                State newState = new State(row, col + 1, mazeArray);
                newState.setCurrentMove(curState);
                stack.push(newState);
                popFlag = true;
            }

            //check if we can move up
            if ((curState.isUp()) && !stack.inStack(row - 1, col)) {
                State newState = new State(row - 1, col, mazeArray);
                newState.setCurrentMove(curState);
                stack.push(newState);
                popFlag = true;
            }

            //check if we can move down
            if ((curState.isDown()) && !stack.inStack(row + 1, col)) {
                State newState = new State(row + 1, col, mazeArray);
                newState.setCurrentMove(curState);
                stack.push(newState);
                popFlag = true;
            }

            // if condition is true, we have reached the exit point
            if (row == height-1 && col == width-1)
                return true;

            if (!popFlag && curState.isVisited())
                stack.pop(solving);
            else if (popFlag)
                solving++;

            if (solving >= stack.getStack().size()) {
                return false;
            }

            if (stack.isEmpty() || solving < 0)
                return false;

            curState.setVisited(true);
            curState = stack.getStack().get(solving);

        }
    }

    /**
     *
     * @return String picturing the maze
     */
    public String toString(){
        String maze = "";
        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                if (mazeArray[i][j]) {
                    maze += " 0 ";
                }
                else
                    maze += " 1 ";
            }
            maze += "\n";
        }
        return maze;
    }

    public void printSolution() {

        // only need this so that while loop won't be infinite
        if (!solve()){
            System.out.println("Maze is insoluble");
            return;
        }

        // this will help putting * on the path
        int mazeSolution[][] = new int[height][width];

        // get the state of the exit point
        State lastBox = stack.getStack().get(solving);


        int row = lastBox.getRow();
        int col = lastBox.getColumn();
        boolean flag = false;


        /** Same as above, I originally had it
         *  if (col == 0 && row == 0)
         * but it exited when either was 0 like an OR operator
         */
        while (!flag) {
            // give every maze box that is in the path the value of 2
            // this way I can use it to print * later
            mazeSolution[row][col] = 2;

            // move backwards to the state I came from
            lastBox = lastBox.getCurrentMove();
            row = lastBox.getRow();
            col = lastBox.getColumn();

            // exit loop condition
            if (col == 0 && row == 0)
                flag = true;
        }
        mazeSolution[0][0] = 2;

        String solution = "";

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (mazeSolution[i][j] < 2) {
                    if (mazeArray[i][j]) {
                        solution += " 0 ";
                    } else
                        solution += " 1 ";
                }
                else {
                    solution += " * ";
                }
            }
            solution += "\n";
        }
        System.out.println(solution);
    }
}
