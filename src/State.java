/**
 * @author Christos Zonios 2194
 */
public class State {

    private int row;        // row identifier
    private int column;     // column identifier
    private boolean left;   // holds whether we can go left or not
    private boolean right;  // holds whether we can go right or not
    private boolean up;     // holds whether we can go up or not
    private boolean down;   // holds whether we can go down or not

    // holds whether the state has been visited. Initialized to false
    private boolean visited = false;

    // State from which we moved to current one
    private State currentMove;

    /**
     * Constructor method
     * @param row - row identifier
     * @param column - column identifier
     * @param maze - input maze - 2d array
     */
    public State(int row, int column, boolean maze[][]) {
        this.row = row;
        this.column = column;
        if ((column == 0) || (!maze[row][column-1]))
            this.left = false;
        else
            this.left = true;

        if ((column >= maze[0].length - 1) || (!maze[row][column+1]))
            this.right = false;
        else
            this.right = true;

        if ((row == 0) || (!maze[row-1][column]))
            this.up = false;
        else
            this.up = true;

        // check if we are in the last row
        if (row >= (maze.length - 1) || (!maze[row+1][column]))
            this.down = false;
        else
            this.down = true;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isVisited() {
        return visited;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }

    public boolean isUp() {
        return up;
    }

    public boolean isDown() {
        return down;
    }

    public State getCurrentMove() {
        return currentMove;
    }

    public void setCurrentMove(State currentMove) {
        this.currentMove = currentMove;
    }
}
