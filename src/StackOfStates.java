import java.util.ArrayList;

/**
 * @author Christos Zonios 2194
 */
public class StackOfStates {

    // the stack is a simple ArrayList object
    private ArrayList<State> stack = new ArrayList<State>(0);

    /**
     * Constructor method
     * @param maze - input maze - 2d array
     * initializes the stack to the state of the maze entry point
     */
    public StackOfStates(boolean maze[][]) {
        stack.add(new State(0, 0, maze));
    }

    /**
     * pop() method
     * @param index - index of stack from which we remove an element
     */
    public void pop(int index){
        stack.remove(index);
    }

    /**
     * push() method
     * @param state - State object which we add to the stack
     */
    public void push(State state){
        stack.add(state);
    }

    /**
     * isEmpty() method
     * @return true if stack is empty. false otherwise.
     */
    public boolean isEmpty(){
        if (stack.size() == 0)
            return true;
        else
            return false;

    }

    /**
     * inStack() method
     * @param row - row identifier
     * @param column - column identifier
     * @return true if item with [row][column] is in stack
     */
    public boolean inStack(int row, int column){
        for (State e: stack) {
            if ((e.getRow() == row) && (e.getColumn() == column))
                    return true;
        }
        return false;
    }

    /**
     * Accessor for the stack.
     * @return the stack.
     */
    public ArrayList<State> getStack() {
        return stack;
    }
}
