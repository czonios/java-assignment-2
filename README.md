# Object-Oriented Techniques
  * **Assignment 2** - *Maze solving*

# Structure
## The project contains 3 classes

### 1) Maze.java

* Contains and handles the maze.

##### Private Fields
* A StackOfStates object called "stack".
* A Boolean 2D array containing the maze.
* The height and width of the maze.
* A helper variable called "solving".
##### Methods
* **solve()** - Checks if the maze has a solution.
* **printSolution()** - Prints the solution to the maze.
* **toString()** - Returns a printable version of the maze.

### 2) State.java

* Current state of a specific point in the maze.

##### Private Fields
* row and column identifiers.
* 4 Boolean variables called left, right, up, down which contain whether we can move to the respective neighboring point in the maze.
* A Boolean called *visited* which is true if we've already visited this State object.
* A State object called "currentMove" which holds the State object we moved to the current one from.
##### Methods
* Various accessor methods.

### 3) StackOfStates.java

* An object manipulating an ArrayList made of State objects, representing a LIFO data structure (stack).

##### Private Fields
* ArrayList of State objects called stack, representing a stack data structure.

##### Methods
* **pop()** - Removes an item from the stack.
* **push()** - Adds an item to the stack.
* **isEmpty()** - Checks if the stack is empty.
* **inStack()** - Checks if item identified by parameters is in the stack.
* Accessor method for the ArrayList object.
